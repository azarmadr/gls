module d_ff(d,clk,q,q_bar);
  input d,clk;
  output q, q_bar;

  wire n1,n2,n3,q_bar_n;
  wire cn,dn,n4,n5,n6;

// First Latch
  not (n1,d);

  nand (n2,d,clk);
  nand (n3,n1,clk);

  nand (dn,q_bar_n,n2);
  nand (q_bar_n,dn,n3);

// Second Latch
  not (cn,clk);

  not (n4,dn);

  nand (n5,dn,cn);
  nand (n6,n4,cn);

  nand (q,q_bar,n5);
  nand (q_bar,q,n6);

  specify
    specparam tSUd = 0;
    specparam tHDd = 0;
    // (negedge clk *> (q:1'b1)) = (0,0);
    $setup(posedge d, negedge clk, tSUd);
    $setup(negedge d, negedge clk, tSUd);
    $hold(posedge d, negedge clk, tHDd);
    $hold(negedge d, negedge clk, tHDd);
  endspecify

endmodule

// Structural model of MUX  
module mux(y, s0, d1, d0);
  output y;
  input s0, d1, d0;
 
  wire s0_bar, d1_m, d0_m;
  not (s_out, s0);
  and (d0_m, s_out, d0);
  and (d1_m, s0, d1);
  or  (y, d0_m, d1_m);
  specify
    (d0 *> y) = (0,0);
    (d1 *> y) = (0,0);
    (s0 *> y) = (0,0);
  endspecify 
/*  table
  // s0 d1 d0 : y
     0  ?  0  : 0;
     0  ?  1  : 1;
     1  0  ?  : 0;
     1  1  ?  : 1;
     x  0  0  : 0;
     x  1  1  : 1;
  endtable*/

endmodule

