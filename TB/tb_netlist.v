`timescale 1ns/10ps
module tb();
 reg a, b;
 wire out;
 reg sel;
 reg clk;

  initial begin
    clk = 0;
    forever #5 clk = ~clk;
  end

 `ifdef GATESIM
   netlist DUT( 
     .a(a), 
     .b(b), 
     .sel(sel), 
     .clk(clk), 
     .out(out));
  `else
    netlist DUT_RTL();
  `endif

  initial begin
   @(negedge clk);
   a = 1;
   b = 0;
   sel = 0;
  end
 
  initial begin
   #100 $finish;
  end

 `ifdef SDF_MAX
   initial begin
    $sdf_annotate("netlist.sdf", DUT);
   end
 `endif

 `ifdef SDF_MIN
   initial begin
    $sdf_annotate("netlist.sdf", DUT);
   end
 `endif

 `ifdef SDF_TYP
   initial begin
    $sdf_annotate("netlist.sdf", DUT);
   end
 `endif

endmodule
