module netlist( a, b, sel, clk, out);
  input a;
  input b;
  input sel;
  input clk;
  output out;

  wire q_a, q_a_bar, q_b, q_b_bar, m_out, out_b;
 
  d_ff f1(.d(a), .clk(clk), .q(q_a), .q_bar(q_a_bar));
  d_ff f2(.d(b), .clk(clk), .q(q_b), .q_bar(q_b_bar));
  mux m1(m_out, sel, q_b, q_a);
  //mux m1(.y(m_out), .s0(sel), .d1(q_b), .d0(q_a));
  d_ff f3(.d(m_out), .clk(clk), .q(out), .q_bar(out_b));

endmodule
