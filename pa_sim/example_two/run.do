# Copyright 1991-2018 Mentor Graphics Corporation
#
# All Rights Reserved.
#
# THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY INFORMATION
# WHICH IS THE PROPERTY OF MENTOR GRAPHICS CORPORATION
# OR ITS LICENSORS AND IS SUBJECT TO LICENSE TERMS.
#
# To run this example, bring up the simulator and type the following at the prompt:
#     do run.do
# or, to run from a shell, type the following at the shell prompt:
#     vsim -do run.do -c
# (omit the "-c" to see the GUI while running from the shell)

onbreak {resume}
#if {[batch_mode]} {
#    echo "Run this test in the GUI mode."
#    quit -f
#}

# Create the library.
if [file exists work] {
    vdel -all
}
vlib work

# Compile the source files.
vlog -work work -f compile_rtl.f

# Optimize the design.
 vopt -work work +acc -pa_enable=highlight+debug \
     rtl_top \
     -pa_defertop \
     -pa_upf rtl_top.upf \
     -pa_genrpt=pa+de+v+u \
     -pa_checks=s+ul+i+r+p+cp+upc+ugc -pa_coverage=state \
     -o dut_opt

# Simulate the design and view the results.
vsim interleaver_tester \
     -vopt_verbose \
     -pa_top "/interleaver_tester/dut" \
     +nowarnTSCALE \
     +nowarnTFMPC \
     -l rtl.log \
     -wlf rtl.wlf \
     -pa \
     -pa_highlight \
     -coverage \
     +notimingchecks \
     -msgmode both \
     -displaymsgmode both \
     -voptargs=+acc

log -r /*
vcd file pa.vcd

vcd add /interleaver_tester/clock1
vcd add /interleaver_tester/clock2
vcd add /interleaver_tester/reset_n
vcd add /interleaver_tester/mc_PWR
vcd add /interleaver_tester/mc_SAVE
vcd add /interleaver_tester/mc_RESTORE
vcd add /interleaver_tester/mc_ISO
vcd add /interleaver_tester/mc_CLK_GATE
vcd add /interleaver_tester/sram_PWR
vcd add -r /interleaver_tester/dut/i0/*
vcd add -r /interleaver_tester/dut/i1/*
vcd add -r /interleaver_tester/dut/mc0/*
vcd add /interleaver_tester/dut/m1/CLK
vcd add /interleaver_tester/dut/m1/PD
vcd add /interleaver_tester/dut/m1/CEB_i
vcd add /interleaver_tester/dut/m1/WEB_i
vcd add /interleaver_tester/dut/m1/A_i
vcd add /interleaver_tester/dut/m1/D
vcd add /interleaver_tester/dut/m1/Q

pa msg -disable -pa_checks=irc
pa msg -stopafter 5 -pa_checks=rsa
pa msg -stopafter 5 -pa_checks=rtc

run -all
pa msg -disable -pa_checks=r

run -continue
#if {![batch_mode]} {
#wave zoomrange 156400ns 185000ns
#}
#coverage report -detail -pa
#coverage save -pa pa.ucdb
#vcover report pa.ucdb -pa

if {[batch_mode]} {
    quit -f
}
