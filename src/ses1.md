# GLS Introduction

## Prerequisites
- Design Verification Concepts
  - DUT, TestBench, Testcases
  - Instantiations and Connecting above components
  - Testplan to cover various aspects of the design
- ASIC flow
- Digital design to debug gate level issues with gates, sequential circuits, CDC cells, FIFO, RAM, etc.
- Timing basics: Setup time, Hold time, Propogation delay
