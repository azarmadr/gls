What Do Gate-Level Sims Buy You These Days?

> We've been looking at what our annotated gate-level sims have in our
> design flow and the cost/return.  We have two extreme views:
>
>    i) that every test case should be run against min/max annotated
>       gate level simulations before tapeout.
>
>   ii) that gate simulations are only a poor attempt to model things
>       which should be covered with static timing analysis and
>       formal verification.
>
> And getting gate level simulations up and running takes work & doesn't
> come for free and debugging them can be painful.  (I can certainly
> speak for Verilog here; I don't know if VHDL eases the burden any.)
>
> So what's the ESNUG users' experience on this?  How heavily do you rely
> on gate sims?  What do they catch that other tools don't?  How do they
> fit into your design flow?
>
>     - Stephen O'Connor
>       Cypress Semiconductor                      San Jose, CA


From: Mark Johnson

Regarding ii) -- I've seen some very careless use of wildcards in STA
  For example, set_false_path -from here* -to there* (especially around low-speed test circuitry) can include real paths you didn't realize are there
  If synthesis doesn't optimize these paths they may wind up slow, but DC will not flag them as violators
  If the same constraints are used in PrimeTime, PT will miss them as well
  STA is only as good as your constraints

set_case_analysis is another constraint that sometimes hangs around (gets
    "reused" in other modes where it doesn't apply) masks problems.  Annotated
gate-level sims can be a good sanity check for missing constraints.

Regarding i) -- It's one way to record switching activity to use for power
estimation.  As far as how many sims to run, it depends on how
representative the tests are of actual operation.  I wouldn't feel compelled
to run every test if I knew  one or two ran my chip in its most power-hungry
modes (and if I had some other assurance that my timing constraints were
    complete).

----    ----    ----    ----    ----    ----   ----

From: Aedan Coffey

Some years ago I felt that gate level sims were not worth the time taken in
doing them, so I put my money where my mouth is and cut them out of the
Eurologic FPGA flow.  OK, it's only FPGAs but you have to start somewhere,
the issues are similar but the the consequences of being wrong are not so
bad.  We did about 30 designs and never had a problem that would have been
caught by gate level sims. We also did one ASIC in this way, but it was a
translation from an FPGA and we used equivalence checking, so it's not the
whole hog.

In my view, if you can do good static timing analysis inside each clock
domain and really careful checking of the clock domain boundaries then you
should be OK and should have saved a lot of work.  There might be some tools
to help with the clock boundary checking but I've not come across them yet.
Where I am at the moment they are using SpyGlass and I'm not impressed, it
does catch things but it also catches so much other junk that it's hard to
see the woods for the trees.

I don't see any difference with this problem in VHDL or Verliog, can you
think of any reasons why there should be?

----    ----    ----    ----    ----    ----   ----

From: Gert Jan Van Loo

Interestingly the exact question was asked by my boss in a meeting two weeks
ago.  We definitely still run gate level sim.  As we are using more and more
formal verification and we have our FPGA emulation system up and running we
do not run gate level sims as extensively as in the past.

Why we do NOT abandon them can best be explained with an example: a few
chips back we had problems because altough STA said the design was clean
our gate-level sims showed many violations.  It took some time comparing
lots of timing values, but we found out why.  The clock in the gate-level
sims was having a much worse mark-space ratio then anticipated.  When the
correct mark-space ratio was fed into STA this also showed the violations.
Lesson: your STA is only as good as the constraints you feed it.  Gate-level
sims show you much better what your real world will look like.

----    ----    ----    ----    ----    ----   ----

From: Norman Stewart

I am a firm believer in static verification.  Static verification
methodologies such as static timing analysis and formal verification are
required to meet aggressive schedules, since they reduce the need to run
full back annotated regression.  However, it must be understood that the
static verification environments are contraint based.  Therefore, the
analysis is only as accurate as the constraints that drive it.

With respect to static vs. dynamic timing verification, I think it is of
primary importance that all asynchronous interfaces are thoroughly stressed
in back annotated simulations.  In most multi-clock domain designs, many or
all of the clocks are treated as false paths in static timing analysis.  I
think back annotated sims are the most dependable method of verifying the
correct implementation of synchronizers and FIFO's at asynchronous
boundaries, and consider back annotated sims a necessary sign-off step
before tapeout.

Regarding your concern about the time and effort required for back annotated
sims.  It's like a bit of an infrastructure issue.  Strict enforcement of
naming rules for asynchronous interfaces will go a long way for you.  Then
you just need a Perl script to modify your SDF's to remove setuphold checks
for appropriate flops.  Your back annotated sims should be drop-in
replacements of the RTL.

----    ----    ----    ----    ----    ----   ----

From: Chris Bohm

Before shipping a design, we run a very limited set of gate-level sims.
Yes, they are time consuming and painful to set up.  Despite this we invest
the time into them,  for the following reasons:

1. They provide a very crude confirmation that synthesis hasn't messed up.
Yes, formal verification is the "correct" way of checking functional
equivalence between RTL and gates, but in the past we didn't have much
fun with formal equivalence checkers.  The design was too big and some
of our instanciated macros turned out to be show stoppers.  However we
feel that we can't just synthesise blindly and ship.  We run the two
or three main modes of operation in a gate level sim to check
functionality. This is a compromise alright, but history shows us that
it's acceptable.

2. They allow to verify scan testing RTL has no concept of scan testing,
gates however do.  Common problems we've seen over the years with scan
insertion are like "IO pads direction not being fixed" or "Asynchronous
resets not being blocked".  A gate level sim in scan mode shows these.

So gate level sims are useful and we'll keep on running them.  But we always
run them to check functionality, NEVER to check timing.  We believe that
timing checks do belong solely into the domain of static timing analysis
tools unless your design style doesn't allow it.

----    ----    ----    ----    ----    ----   ----

From: Thomas Fairbairn

Interestingly we had exactly the same conversation on our last project:
why do gate level simulation?  The conclusion we came to is that we can't
get rid of it.  Here's what we get from it:

1) A nice warm fuzzy feeling :-)
2) It's a good way to test your STA set up.  For instance, are you 100%
sure all your false paths are actually false?  Reviewing the timing
violations from your simulation is a good way to check you've fully
captured the constraints you need.
3) Similarly, it helps validate your formal verification.  For instance,
are your set_comparison and set_equivalent points right?

There are other ways to check these things, I'm sure, but actually doing
the simulation ensures that you haven't forgotten anything.

The disadvantages (all fairly obvious):

  1) Takes a lot of setting up and debugging
  2) Takes a huge amount of computing resources (CPU time, disk space for
    the waveforms, etc.)

  So, if you're startup with limited time, people and computing resources
  then I'd consider not running gate-level simulations at all.   If the risk
  isn't worth saving e.g.  2 weeks of simulation, then you have to do it.

  A 3rd way, of course, is to do the simulations anyway but start the
  backend process at the same time as your simulations.  That way if there
  is a problem you can always re-start the backend, but if there isn't, you've
  saved yourself some market window.

----    ----    ----    ----    ----    ----   ----

From: Brian Kuebert

Gate level simulations, while they do have their place, only represent a
very small slice of what should actually be tested for at tapeout.  And on
multi-million gate designs the time to complete anything other than the
most rudimentary fully back annotated simulations is schedule prohibitive.

Gate level sims do have their place, though.  I see them having two key
purposes.  First, they offer a warm and fuzzy feeling that what you're
going to get back will actually work.  Although this might not seem that
important at first glance, if you've been working on a multi-year,
multi-million dollar project anything you can reasonably do to help ensure
success is useful.  Second, gate level simulations can offer some (albeit
    small) confidence that your static timing constraints are correct.  After
placing and routing a design and finding some false_paths that weren't
really false and some multi_cycle_paths that weren't really multi-cycle
you, too, would be a believer in the sanity check gate level sims provide.
Admittedly there is the same argument that the simulation space is so
large you'll never cover everything (in terms of STA constraints), but, if
you wisely target your gate level sims -- to particularly tricky parts of
the design or parts designed by newbies -- you can at least get some
confidence.

The bottom line for me is that timing back annotated simulations should
not be required yet they still do have their place -- mostly as a
confidence builder.  STA and formal verification should render their need
obsolete, however, my experience has shown that even the most thorough
analyses still may have holes and gate level sims can serve as a sanity
checking cross-check.

----    ----    ----    ----    ----    ----   ----

From: Greg Arena

I've only been doing this for five years, but here is my experience with
doing annotated gate-level simulations in addition to the static timing
analysis and formal verification:

- I recently provided assistance to one team of ASIC designers in verifying *  a rather large bridge-type design. The gate-level simulation there
uncovered problems with shorter pulses being "swallowed" by some of the
slower gates which, to my recollection, the designers were not able to
detect using PrimeTime. Perhaps we were using an older version of the
software or maybe we didn't have it configured correctly to detect this
sort of thing, but I do recall seeing it in the gate-level sims and the
senior design engineer saying he had already found it the same way when
I reported it to him.
- On the same design, the gate-level sim also uncovered instability on a *  high-speed output bus due to similar timing violations that were not
picked up by PrimeTime.
- On some occasions, I've marked false/multicycle paths that *weren't* *  really what I thought they were and the gate-level simulation picked up*
on it.
- The reverse has also happened to me: the static timing analysis will show *  failing path(s), but closer study of that path in the gate-level
simulation reveals that it's not really a problem after all.

To be honest, it makes me feel safer about signing off a design if I see
that it can run the same simulation at the gate-level as well as on the
RTL.  I think STA is great, but I still want to see the thing run a
simulation of what it's going to experience when it's installed on its
target board before I put my signature on those sign-off documents.

----    ----    ----    ----    ----    ----   ----

From: William Natter

The primary purpose of Static Timing Verification should be to get most
timing checks covered, in most of the situations they will face.  If
your chip has system clocks that only talk synchronously with each
other, works in a single mode of operation all the time, and your Static
Timing Analysis (STA) setup includes no constants, disabled arcs or
false paths, then you can cover everything in STA.

Note that if you have on-chip testing, you already will have to apply
constants (case analysis in PrimeTime).  Mind you, those paths you set
as constant this way may not be very sensitive to timing.  Also, if you
have several clocks, where some of them talk to each other through
"asynchronous" circuitry (typically some device where functionality and
    timing cannot be separated), then once again you will have to cover that
in simulation or somewhere else.

----    ----    ----    ----    ----    ----   ----

From: [ Thing 1 ]

John,

Please keep me anon.  My response to Stephen O'Connor's question
about gate-level sims is they aren't necessary, *if* you have 100% *
confidence in the static timing and formal verification tools and
the people who write the scripts for them.  Since I've never worked
anywhere that had that level of confidence, I've always seen
gate-level sims run.  As a compromise between running every test
and none at all, most companies I've seen run a small number of
tests that are judged to test the highest percentage of the chip.
Yes, it can be painful to set up, but to me it is worth it to be
able to sleep at night after tape-out knowing you have done
everything possible to ensure the chip works.

Gate-level sims found a timing problem at the last company I
consulted with.  The design used a flop to divide the core clock
down to a half-speed clock.  The person who did the PrimeTime
scripts didn't know about the create_generated_clock command and
used create_clock and set_clock_latency commands instead to define
the half-speed clock, but got the latency value wrong.  As a result,
PrimeTime reported no violations but the gate sim found violations
where signals from the core clock domain were clocked into the
half-speed clock domain.

----    ----    ----    ----    ----    ----   ----

From: Mark Curry

Well, for us we've gone now two generations of designs with very minimal
gate level sims at all before tapeout.  It's pretty much unavoidable.

- Gate level sims won't fit in a 32 bit binary.  Sure most tools do *  have 64 bit versions now. But, with all of the IP we receive in
various forms (PLI models, ModelManager, etc..), at least some of
them still lack 64 bit support.

- Generation of debug data (VCD, SignalScan, DeBussy), is nearly *  impossible with gates.  The design databases hit 10s to 100s of GIGA
bytes before you can even blink your eye.  Heck we're even having
issues with this on RTL sims.  We need to slice and dice the database
in time or hierachy to get anything the tools can handle.

Some important rules to make this successful.  Your STA must be carefully
reviewed, and complete.  Be very careful with ANY exceptions -- multicycle
paths, false-paths, synchonizers, etc.  We've started to push this back on
the designers more, by having them instanciate specific modules to give STA
a tag to work off of.

i.e. we have standard synchronizer modules.  So, STA scripts can key off
these modules.  We also have "dummy" synchronizer modules.  For those that
take care of the synchronization through the protocol or other means - the
cross clock signals go through a "toolbox_sync_feedthrough" module.  It
just passes the wires in->out, but again, give a key to STA.  This forces
the designer to think about it, and document it when he's designing the
block.  Instead of the STA guy trying to reverse engineer it months later.

VERY special care must be given to clock, and reset generation.  As most
designers know, clock and reset generation is as much art as it is design.
We've had a different person do this block for each design so far.  Every
time, we go through many deep code reviews of this, with everyone in
attendance - synthesis folks, layout folks, STA folks, designers, DFT,...
In the end there's usually 10-20 significant revisions of the chip clock
and reset strategy.

Another note -- all FFs in a design should have a reset.  Be it asynchronous
or synchronous, as long as it can be reset.  I often thought couldn't we
save some gates, by not resetting our datapath FFs, just control.  Well it
just doesn't work well.  X's in RTL simulation are bad news.  They should
be avoided at all costs.  X's can be pessimistic OR they can be optimistic.
Take the standard if-else.

always @( blah )
if( condition )
signal = 0;
else
signal = 1;

Well what if condition is X?  You got it, signal get's a 1 in RTL sim.
Sim vs. synthesis mismatch.  So, allow no X's in RTL sim.  Inititialize
everything to a known state.  Do not assign X's anywhere.  They're not
pessimistic.  The gate savings for don't cares is in the noise for these
size designs.

Yes, you still need to deal with uninitialized memories.  Just be careful.

In the end we usually are pushed to do some gate level sims.  It's for that
"Warm Fuzzy Feeling" that all is well.  It addresses those unknowns; did we
get the clock and reset right?  Were all those STA exceptions valid?
Usually this is a minimal sim.  Usually, for us, it's started just before
tapeout, and actually doesn't finish until AFTER we've already pulled the
trigger.  Risky?  You bet.

It's just a balance.  The gate level sims, even for a minimal sim, takes
literally weeks to get going.  The designs are just too large.  Here you
have a design which passes RTL verification sims.  The layout team, after
months of work solving these nasty deep sub-micron issues, gives the thumbs
up.  STA shows everything OK, over MANY corners.  FV is clean.  "What about
gate level sims?" someone asks.  Another 4-6 weeks of sims at least.  You
can't start it sooner -- why bother with an SDF that layout/STA hasn't
declared "clean"?

We've had the latest silicon back in our labs for a couple of weeks now.
Everything's been looking great so far.  We have not had any issues in the
past specifically related to not having done significant gate-level sims.

----    ----    ----    ----    ----    ----   ----

From: Leo Butler

We all loathe gate-level simulations.  We want to banish them from our
lives... yet still they are with us.

Given that STA is a very manual, often complex process, we've found gate-
level simulations to be the best way to close the loop.  It is a poor way
to catch problems, but if you set up your environment with an eye towards
making the task easier, it can be invaluable.  Reset sequences are also
more problematic in gate-level simulations, so this is a common place to
catch them.  Synchronous reset issues that cause unknowns to pass into the
flops when the reset is synthesized as part of the logic can be caught
explicitly in gate-level simulation, although there are some static
verification tools that will flag this.  Still, if you are going to run
gatesim anyway, it will likely pop up if it's an issue.

Validation of cross-clock domain false/multicycle paths, user defined modes
of operation with case_analysis and clock phase relationships can be
error-prone.  I wish there was another way to validate this, but the two
methods at our disposal are thorough review of STA constraints (a task that
never seems to be done) and gate-level simulation.  Nevertheless, for any
bugs you find in gatesims, you should assume there are more lurking in
there... gatesims are notoriously incomplete with regards to what can be
tested under the right conditions vs. what the environment will actually
catch prior to tapeout.

Start early; make gate-level simulations a priority early in the design
cycle.  Back-annotation isn't necessary to catch all problems; intrinsic
delay and idealized clocks make for occasionally messy simulation issues
but they can often be worked around.  And figure out a mechanism to handle
your synchronization flops -- you'll need to shut off "X" prop on the first
level sync flop somehow.

It's getting harder and harder to handle gate-level simulation with the size
of the designs and complexity of the verification environment, but there are
still few (if any) alternatives.

----    ----    ----    ----    ----    ----   ----

From: Jonah Probell

Since formal verification checks one model against another, it depends on
the reference model having been correctly verified.  Gate-level sims are
a way of testing the testbed on another model.  I have found testbed bugs
through the process of bringing-up gate-level sims, and I think the practice
has value.

----    ----    ----    ----    ----    ----   ----

From: Doron Nisenbaum

Well, this has been an issue in all the places I have worked in.
Theoretically, you do not need the Gate-Level sims any more.  As Steven
wrote, for Timing Analysis you use static timing tools and for
behavioral  the gate-level sims are needed mainly to verify any
environment and initialization issues.  Behavioral models might hide
problems in some kinds of special modules like memories, counters with
no reset and modules that might start the simulation full with "X" values.
Those problems might be all "simulation" problems, but it is good to be
aware of.

----    ----    ----    ----    ----    ----   ----

From: Shyam Pullela

1. We always run gate-sims (no time delays) to make sure that some of
the black-boxed logic in FormalDV is connected properly.
2. Another reason is to make sure that there are no RTL vs Synthesised
gate diffs due to simulations.  How much ever we try to stick to good
coding guidelines we always found bugs in gate-sims due to this aspect.
3. We never run gate-sims on all the tests. We run them only on few
representative tests.

We never send out chips without gate-sims due to 1 and 2 above.

----    ----    ----    ----    ----    ----   ----

From: Mitch Dale

Read Stephen's post in ESNUG this morning.  This is a similar issue with us.
As a startup, we're still putting methodology in place.  My response:

1) A couple of gate-level sims are good for verifying (or at least
improving one's confidence in) the accuracy of Static timing and
equivalence checking (including: are there any discrepancies between
the different vendor library models for static timing, equivalence
checking, and simulation).

2) Gate-level sims are also good for verifying the functionality and
timing of circuits an paths not covered by static timing (e.g.,
asynchronous interfaces circuits, cross-clock domain paths, etc.)

I'll be interested as to what other's come up with.

----    ----    ----    ----    ----    ----   ----

From: John Weiland

Yes, gate-level sims are a huge pain in the whatever.  I can think of a
couple of questions you might want to answer when deciding whether you
can skip them:

1. How close is your design to the simple-minded ideal of a fully
synchronous circuit with a single clock?  The more interfaces between
clock domains, asynchronous logic, etc. that it has, the less I would
trust a static timing analyzer by itself.  On the other hand, coming
up with a simulation that checks all the corners will also be a thrill.
Ideally you'd like to check this stuff at the block level but these
scary interfaces often show up between blocks.

2. How many false paths does it have?  Lots of false paths means lots of
opportunities to accidentally label a path as false that really isn't.
I've seen this one in real life.

Gate-levels sims aren't going away any time soon.

----    ----    ----    ----    ----    ----   ----

From: Francisco Javier Alonso

In principle, 100% gate level sims verification is impossible.  But is no
less true that no gate level simulation, only formal check and STA, is
not realistic either.

We are not in an ideal world.  I mean, what I try to do is avoid any naked
RTL construct that mask X propagation.  (I include X detection and
propagation in my RTL between SYNOPSYS "translate off" and "on" pragmas)
so reuse of testbenches for gate level sims are less painful to setup.

Then, I try to complete STA/formal and during any time left, I spend it on
gate level sims.  I always find something doing the last ones that was
overlooked in the other verification.  Why?  Everything is so complex that
putting a simple typo in your STA script and you can be expecting to see
"funny" silicon results if you disregard gate level.

Anyway, nothing is perfect and sleeping well after a tapeout is never going
to happen, anyway.
