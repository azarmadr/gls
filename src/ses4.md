# GLS Issues


## Revision
### The typical RTL-to-gate-level-netlist flow is
```mermaid
graph TD;
  A[\RTL Design/]
  B[Synthesis]
  C{Logic<br>Equivalence<br>Check}
  D[/Gate-Level Netlist\]
  A --> B --> D
  A <--> C <--> D
  subgraph RTL
    Testbench --- A
    Verification --- A
    Linting --- A
  end
  subgraph GLS
    D --- X[ATPG Pattern]
    D --- Simulation
    D --- STA
  end
```

### GLS verification
- Verifying netlist which is in the form of gates
- TB setup of RTL will be modified upon
- done on
  - DC - Synthesized netlist *Design Compiler*
  - PD - Place and Route netlist
    - design into a representation of the actual geometries we manufacture


## Why do GLS?
What RTL lacks? |What LEC lacks? |What STA lacks? 
:--|:--|:--
1. Linting Bugs | 1. Delta-Delay Race Conditions  |1. Timing Bugs 
2. BFM-masked Bugs |2. `'ifdef` Bugs|2. Reset Timing Bugs 
3. IP Bugs |3. LEC Holes |3. Dynamic Frequency Change Clock Bugs
4. Clocking Bugs |4. LEC Waivers |4. Multi-Cycle Path (MCP) Bugs 
5. Force/Release Bugs |5. Clock-tree synthesis |5. Asynchronous Interfaces 
6. BIST/BISR Bugs ||
7. DFT Bugs ||
8. Power Insertion Bugs ||
9. Analog models ||

### What RTL Verification lacks?
#### 1. Linting Bugs
  - Lint tools like Real Intent or Spyglass look at your source Verilog/SystemVerilog/VHDL RTL code that produces bad gates
  - The bad news is that some lint tools (I won't say which ones) have a signal-to-noise ratio that produce too many warnings that need to be reviewed and waived by hand
  - The human error of waiving the wrong lint warning creates a difference between RTL and gate functionality
  - And worst, LEC will not find these waivered bugs since LEC starts with the same wrong gate functionality.
#### 2. BFM-masked Bugs
  - RTL verification typically uses BFMs (Bus Functional Models) to simplify test generation and checking of results
  - BFMs that incorrectly model part of your DUT can cause bugs to be missed
  - Your GLS must do some tests driven by gate-level cores instead of just internal BFMs.
#### 3. IP Bugs
  - You can have 3rd party IP that works perfectly in RTL, but quietly contains timing/functional/ifdef/pragma bugs that can only be caught in GLS
  - These quiet IP bugs can kill a chip.
#### 4. Clocking Bugs
  - Your RTL has quiet real-life glitches, over max frequencies, or duty cycle bugs are often only seen in GLS tests with full SDF timing.
#### 5. Force/Release Bugs
  - Often in testbenches to get past some bottleneck, code like this will be used: force load_fifo_name_here = 1'b1; force ecc_error = 1'b0; force aix_bus = 32'bFFFFFFFF; What happens is the verification guys forget to remove or "release" all or some of these "force" commands -- causing tests to pass and their bugs to go undetected
  - GLS throws up compile errors for most internal "forces" when signals are renamed through synthesis; with the few "forces" remaining to be reviewed and removed if possible.
#### 6. BIST/BISR Bugs
  - Built-In Self Test/Repair
  - If your design's original Veilog/VHDL source RTL does not include BISR or BIST logic, bugs involving the BIST/BISR logic can only be found in GLS.
  - Validate constrains defined during formal verification whilescan and MEMBIST insertion happens after synthesis
#### 7. DFT Bugs
  - Usually RTL does not include DFT logic so those bugs in the DFT logic can only be found in GLS.
  - DFT verification, since scan-chains are inserted after RTL synthesis
#### 8. Power Insertion Bugs
  - Usually your RTL is not power inserted
  - UPF testing is an attempt to find these in RTL, but since most power logic is not included in RTL, the only true test of power logic can only be done with a "power aware" gate-level simulation
  - This is where the simulation models of your gate-level library cells only work when your power-enabled netlist are connected and driven correctly by your clamp cells, voltage translators, and power islands.
  - Realistic power analysis on netlist with delays
  - To verify low power structures absent in RTL and added during synthesis (power aware simulations)
#### 9. Analog models
  - Analog model interaction with digital in conjunction to timing and delays
  - To collect switching activity for power estimation and correlation.
  - Apart from logical netlist even physical netlist (with VDD & GND) needs to be verified here

### What LEC lacks?
#### 1. `'ifdef` Bugs
  - From `'ifdef`s in your code where RTL simulation uses one set of `'ifdef`s different from the `'ifdef`s synthesis used
  - LEC does not catch this and you won't suspect anything until you run GLS.
#### 2. Delta-Delay Race Conditions
  - Occasionally RTL is run with #0 or #1 or blocking and non-blocking assignments that include a RTL delta delay race condition
  - These are simulation artifacts
  - If your source RTL simulates wrong, people will design their chip to "pass" wrong
  - They are assuming everything is OK
  - But their final Gates will work differently than their RTL -- and this is a rare case where only GLS will detect the real silicon behavior mismatch.
#### 3. LEC Holes
  - All LEC tools work by doing a logical equivalence between two gate-level models
  - If you're doing LEC between your RTL and your Gates of the same design, the LEC tool starts by doing a synthesis of your RTL to a simple gate implementation
  - If your "RTL" gates does not 100% synthesize to gates that 100% match your RTL's functionality, your LEC run will be comparing your Gate netlist with an "RTL" gate model that is already broken
  - You can get incorrect LEC results from this.
  - To validate EDA tool flow change while moving from one vendor’s sign off tool to another.
  - Multi vendor EDA tools flow is common
  - Sometimes to direct the synthesis, RTL instantiates a module from the synthesis tool vendor library for which an equivalent is hard to find in a competitor’s EC tool
  - Also required to simulate ATPG patterns
  - To generate ATE(*Automatic test equipment*) test vectors.
#### 4. LEC Waivers
  - Large designs are divided into pieces to allow LEC to handle it within a reasonable time
  - Any tool mistake in this cutting process or any incorrect waivers can result in functional differences between RTL and Gates
  - Only GLS detects this.
#### 5. [Clock-tree synthesis](https://www.vlsiguide.com/2018/07/clock-tree-synthesis-cts.html)
  - the inclusion of clock tree buffers and other normal buffers at the time of optimization are done during the post-layout-netlist.
- Analyzing X state pessimism or an optimistic view, in RTL or GLS

### What STA lacks?
#### 1. Timing Bugs
  - Using incorrect constraints actually cause your DC or Genus synthesis tool to create timing bugs -- and then those same bad constraints are used to run Primetime or Tempus STA
  - So the same constraint error will cause both the bug and the bad check that will miss detecting that bug.
  - Static verification tools are constraint based and they are only as good as the constraint supplied
  - Constraints might set et false and multi cycle paths where they don't belong
  - Careless use of wildcards, typos, design changes not propogating to constraints or incorrect understanding of the design all demand validation of these constraints
#### 2. Reset Timing Bugs
  - These are typically clock zones where the reset is released at different clock edges on their D-FF's
  - These are also called initialization bugs
  - They can only be detected in gate simulations with delays.
#### 3. Dynamic Frequency Change Clock Bugs
  - Often high performance, yet low power chips must be able to switch frequencies without quiescing its logic
  - This logic can only be verified with GLS with full timing to detect these clock issues.
#### 4. Multi-Cycle Path (MCP) Bugs
  - For example, you have a chip with a 12-cycle MCP in it
  - a) your source signals must be held stable for the full 12-cycle period, and b) your destination flops must only capture the results at the 12th cycle -- and not earlier nor later
  - If you fail to do "a" or "b" above, it will create an MCP set-up/hold issue that causes metastability ("X's") on your final output flop.
#### 5. Asynchronous Interfaces
  - The inability of STA to identify asynchronous interfaces
  - To verify critical timing paths of asynchronous designs that are skipped by STA.
Note:
  - 3.2 wrong understanding of the design may lead to wrong false path definition

## GLS challenges

### Immensity
- Long compilation time for large netlist + SDF Annotations
  - GLS with timing is inherently slow
  - Careful planning is required to decide the list of vectors to be simulated on netlist, turn around time for these tests and memory requirements for dump and logs.
  - Large simulation runtimes *even 3-4 days or a week*
- PLL Phase-locked loop Decreasing Technology nodes:
  * PLL are used to generate clocks - Digital PLL have high clock-jitter unlike the Analog counterpart
  * 90nm -> 65nm -> 45nm -> 28nm -> 16nm
  * Different nodes often imply different circuit generations and architectures
  * Generally, the smaller the technology node means the smaller the feature size, producing smaller transistors which are both faster and more power-efficient
- Large Compile DB sizes which translate to
  * Large Machine Memory requirements
  * Frequent simulation hangs
- Identifying the optimal list of tests to efficiently utilize GLS in discovering functional or timing bugs is tough
  - Coverage/Assertions dedicated for GLS are required to assure completeness of these tests.

### Synthesis
- Prelimnary netlist simulations without timing are prone to race conditions
  - With improperly balanced clock tree, sometimes data propogation to next stage happens before clock leading to unusual results
  - A lot (hierarchy, net naming etc) changes with each netlist release leading to a lot of rework on the force statements (if any remaining) as well as the assertions/monitors
- During clock tree synthesis, the clock port/net is replaced with a clock tree with buffers/invertors to balance the clocks
  * Monitors/Assertions hooked to the clock port in test bench during RTL simulations need to be revised for GLS to make sure the intended net is getting probed
- Complex SDF timing annotations
  * For netlist simulations with timing check, one needs to remove the synchronizer setup-hold constraints from the SDF file or disable timing check on these component instances
  * Getting a list of these synchronizers is a dauting task if certain coding conventions aren’t followed

### Debug
- GLS Debug is complex and time consuming
  * False failures due to `x-propogation`
  * During the initial stages of GLS, identifying the list of flops/latches that needs to be initialized (forced reset) is a bug hurdle
  * Simulation pessimism would drive out X on the output of all such components without proper reset bringing GLS to a standstill
- Assertions are increasingly used for functional verification to improve observability and error detection
  * Reusing the assertions for GLS raises reliability concerns due to issues arising from incorrect clock connections or negative hold time causing assertion sampling at undesirable time
- Debugging the netlist simulations is one of the biggest challenge
  * In GLS, ‘X’s are generated if there is a violation of timing requirements on any of the netlist components or when a given input condition is not declared in a UDP’s (User defined primitives) table
  * Identifying the right source of the problem requires probing the waveforms at length which means huge dump files or rerunning simulations multiple times to get the right timing window of failure
  * Engineers tend to get lost easily during the debugging of X propogation source.

## GLS setup
- Netlist design
  * Should have passed equivalence check(Formally Clean)
- Technology library models
  * Gates, flops, HardMacro(IC in block form)
  * UDP - User Defined Primitives
- SDF file for timing annotations corresponding to min and max corner
  * Must include post layout interconnect delays
  * All values for min/typ/max should be present in SDF. If not, the suitable option should be provided with compiling option.
  * SDF should be consistent with the design netlist for simulation.
  * SDF should also be consistent with the timing models of cell
- Non-resettable flops list
  - Probably from STA engineer
  - Using scripts to search them ourselves
- Special models for Analog/Mixed Signals
   - PLL (Phase-locked loop)
- Test bench for Functional Simulation
Note:
- error during annotation (same design hierarchies and port list should be present)
- Values for SDF Annotations(For example with VCS If value for typical delay is missing then we need to use +max/min_delay switch with compilation).
- An SDF file contains only timing data. It does not contain instructions to the analysis tool concerning how to model the timing properties of the design. The SDF keywords and constructs that surround the data in the file describe the timing relationships between elements in the design only so that the data can be identified by the annotator and applied to the timing model in the correct way. It is assumed that the timing models(specify block in UDP or definition of the cell) used by the design are described to the analysis tool by some means other than the SDF file. Thus, when using SDF, it is crucial that the data in the SDF file be consistent with the timing models.
- For example, if the SDF file identifies an occurrence of a 2-input NAND gate ASIC library cell in the design and states that the input-output path delay from the A input to the Y output is 0.34ns, then it is imperative that the timing model for this cell has an input port A, an output port Y and that the cell’s delays are described in terms of pin-to-pin delays (as opposed to distributed delays or a single all-inputs-to-the-output delay).

## GLS steps
- SV environment for GLS
  - `'GATESIM` switch to instantiate Netlist DUT
  - Adding `'GATESIM` wherever we refer RTL hierarchy
    - Optimized by the sysnthesis tool
  - Testcases, asserstions and memory paths differ with RTL
  - Copy suitable libraries(udp or other form) to one place which contain component definitions used inside netlist(Gate Level Design)
    - These component definition also include specify block with timing information.
- Changing paths for preloading memories (RAM)
  - Compiled C code, *.hex form*, should be loaded into from where the processor starts fetching data
    - On-chip RAM, DDR RAM, Code-RAM, Data-RAM
    - Hierarchy of these memories may change during sysnthesis
    Note: 
    1. C-code, Hex, or Assembly Code GLS Tests
       These are code stimulus and checking tests are written in C and
       then compiled to assembly hex files that are checked into your
       repository.  The testbench releases reset on the DUT, and loads
       this hex file into the external RAM or ROM model in the testbench,
       or back-door loads an SRAM inside the DUT. The testbench releases
       reset on the DUT and waits for a handshake.

       The handshake is usually a location in memory that both the DUT
       and the testbench can access, or some use of IO's to communicate
       between the testbench and DUT.  The DUT boots from the hex code
       and performs the test on it's own.

       NOTE: These hex tests should bring any peripherals out of reset,
       and check that RAMs, registers, DDR, peripherals, etc. can be all
       read and written correctly through all of the interconnect in
       your chip.  This is done with a write followed by a read so the
       hex code can self-check.

       If data is correct, it goes on.  If incorrect, the test fails
       immediately by writing the handshake PASS/FAIL location that the
       test has FAILED, and the DONE handshake location that the test is
       finished.  Meanwhile the testbench verification thread has been
       polling the DONE location since releasing reset and when it is
       seen to be true, finishes the test, writing out PASS/FAIL and as
       much information as possible to the log file.

    2. GLS BFM tests
       These are Monitor checking tests that use BFMs mixed in with your
       GLS to stimulate and check correctness.  These tests are easier to
       write and debug than assembly hex code driven tests because they're
       usually leveraged from your earlier RTL regression.

       The catch is that they require Cross Module References (forces and
       probes) into your DUT -- which is very tricky with Gate Simulation.
       Your gate netlist must preserve all of the signals to be probed and
       forced in each version, or there must be a mapping file for each
       netlist that can be easily used to connect the signals to the
       testbench.
- SDF Annotations
  - Copy SDF file to working location
  - Add the SDF annotation command to the elaboration option wrt to simulation tool used and along with the associated SDF file location (for example if VCS is used : -sdf max:TOP_DUT_NAME:max_sdf_file_path)
- Adding Force file
  - `.do` files prepared by GLS engineer from a list of non-resettable flops *STA engineers*
  - contains syntax to initialize non-resettable flops and memory outputs
  - force `-feeze` and `-deposit`
  - can be used to address simulation hangs by forcing some flop values
    - `x` propogation from memory models can be identified by forcing `NO_CORRUPT` bit on memory
    - `clock` enables can also be forced

## GLS Testcase Selection
- We don't run entire RTL regression, 
  *because* GLS is time consuming making sure to cover
  - Atleast one testcase for each block in the design
  - Digital - Analog boundaries
- Must involve boot up or initialization
  - Chip coming out of reset
  - Functional in different boot modes
  - To check entry/exit from different modes of the design
- Clocks
  - Clock checking and source switching
  - Clock frequency scaling
  - Patterns covering multi clock domain paths in the design
- STA
  - Asynchronous paths in design
  - Dedicated tests for timing exceptions in the STA

SoC Verification | GLS Verification
---|---
CPU-SS<br>Power Manager<br>Peripherals<br>Buses<br>Multi-media SubSystem| Booting<br>Cache access with MMU enabled<br>Interrupt handling (Generic Interrupt controller)<br>Power collapse entry and exit
200-300 testcases | 20-30 testcases 
Note:
- SOC Testcases will target different features of the CPU's 
- In GLS, Peripherals may have one testcase each *likeUART, I2C, SPI*

## GLS types
Zero / Unit delay simulation | Timing simulation 
--|---
No Annotations | SDF Annotations 
Early clean-up and bring up of netlist in the early design cycle | behaviour simulating real Silicon(Si) 
Before SDF is ready or annotations added | Typically requires waiting for all the IP blocks or even the standard delay format (SDF) file are ready 
Has a slower compile and run -- especially when dumping waves| Slowest compile by far.  Simulation speed a little worse than 0-delay Gates.
Checks reset sequence and system boot<br>Checks if there are issues due to scan insertion<br>Can find zero delay loops and thus race conditions in the design| SDF provides timing delay values for all cells and interconnects<br>min: best delay for ideal Process Voltage Temperature *PVT* conditions<br>- for hold timing checks<br>max: worst case timing delay<br>- for setup timing checks<br>typical: intermediate of min & max

## GLS Issues

### TB compile time Setup issues
- Due to design optimization by sysnthesis tool
- Using improper `casex` or `casez` can lead Synthesis tool to do improper sysnthesis
- Nomenclature of hierarchies and signals may change
  - GenVar
  - Bus signals getting bit blasted
  - change/removal of signals due to optimization
- Use Probes and Forces only on Physical Block Boundaries
  - Probing internal signals from the testbench is not doable
  - This also impacts using "force" commands, too
Note:
- For case related use `unique` SystemVerilog keyword
- Plan for this by having your Physical Design team have their synthesis scripts keep the names of the signals that you want to probe from the testbench
- Most PD groups do this by preserving all of the ports on the physical block boundaries
- Then you the test guys must follow the rule that all testbench interfaces must connect only to ports on these blocks

### X-propogation
- `x` optimism refers to how simulations may incorrectly exhibit determinate behaviour even when inputs to logic are X and have indeterminate value.
  - It can be dangerous and can mask real RTL bugs
  - `posedge` and `negedge` from `x and z`
- `x` pessimism: drives `'z` or `'x` even in determinate situations where having `x` is not ambiguous
```verilog [1-10,13,15|11,12]
primitive MUX2 (Z,D0,D1,SD);
  output Z;
  input
  D0,D1,SD;
  table
  // D0 D1 SD : Z ;
     1  ?  0  : 1 ;
     0  ?  0  : 0 ;
     ?  1  1  : 1 ;
     ?  0  1  : 0 ;
     0  0  x  : 0 ; //If absent then this library
     1  1  x  : 1 ; //is X pessimistic
  endtable
endprimitive
```

#### Causes
##### Library models
- Each gate has a corresponding library model containing its functionality
```do
+libext+.v #VCS
#in Questa probably do with 
#vlib lib_name
#vlog -f
-y /techlibs/your_technology/v6.0/verilog/
-v /techlibs/your_technology/v6.0/verilog/cell_udps.v
-y /techlibs/your_technology/v6.0_rams/verilog/
-y /techlibs/your_technology/v6.0_custom/verilog/
```
- Models might have `specify` blocks which might add delays or use `#delay`
```verilog
module BUFX2 (Z, A);
output Z;
input A;
  buf U0(Z, A);
  specify
    (A *> Z) = (1.0, 1.0);
  endspecify
endmodule
```
- They prevent x to updated or initialized in a given timing event
- Can be disabled with `+nospecify` or `+notimingchecks | +ntcnotchks` flags

##### Design
- Non-resettable flops can't break from `x-propogation`
[clk/2 nrf](./img/clk-by2-nrf.png)
[clk/2 nrf w/logic](./img/clk-by2-nrf-wl.png)
- Race Condition in Transfer to Divide by 2 clock domain
[clock div](./img/clk-div.png)[race](./img/race-clk.png)[race with sdf](./img/race-clk-sdf.png)

##### Synthesis
- Resets might be boggled by logic
- Incomplete Logic Optimization Interferes With Reset

### 0-delay Simulation issues
#### Zero delay loops
- Tricky Delta-Delay Races in 0-delay GLS runs
  - Improper DFF simulation
  - RTL verification is functionally blind to this
  - Around the signal assignment
    ```verilog
    logic clk,sig_a;
    initial forever #5 clk = ~clk;
    @(posedge clk) sig_a <= ~sig_a;
    ```
  - Clock dividers are used to generate clocks
  - Assertions can be used to identify those DFFs
Note:
  When your Veilog simulator does not simulate a DFF correctly it's called a delta-delay race.  Instead of taking the value on the D-input *before* the clock edge, it accidentally uses the D-input value *after* the clock edge.  It looks like a fast-path hold failure in simulation where the Din and Qout change at the same time<br>
  This is a silent chip-killer if it happens in your RTL simulation; because your RTL functionality is different from the functionality of your synthesized logic.  Lint doesn't catch this.  LEC doesn't catch this.  The only 100% sure way to catch this is through GLS SDF runs<br>
  These issues often occur in 0-delay GLS because with 0-delay, all the functional signals are changing at the same time as the clock edges.  Simulators deal with this by putting the rising edge of the clock in the blocking portion of the zero time window (early) while the functional signals are restricted to the NBA (non-blocking assign, or late) half of the zero time window.  The way the simulator knows how to do this is by looking at how the signal is generated.  Clocks are supposed to be generated by a blocking assign (=), while functional logic Qout's are generated by a non-blocking assign (<=)<br>
  Unfortunately since most clocks today are generated by a clock divider which uses DFF's, the clocks are generated by the Qout of a DFF.  And these are modeled in gates with a DFF UDP (User Defined Primitive).  Originally UDP DFF models defaulted to blocking assigns, but a few years ago, the Verilog LRM changed the default to non-blocking assign<br>
  Either way, having the clocks and functional DFFs driven by the same DFF simulation model leaves a problem.  How do you tell the simulator which is a clock and which is not?  Simulators typically get it right most of the time.  Which makes me suspect the tool is identifying clocks during the compile of the design, and making sure those signals are always in the Blocking Assign region<br>
  But there always seem to be a few cases -- especially with hard macros, or weirdly coded RAM models, where 0-delay GLS fails due to delta-delay races.  Typically when these fails are found, they can be fixed with a *separate* model for DFFs driving clocks.   Another fix is to put a #delay on the input of the D-in, but the *separate* model approach will fix all DFFs on clock nets<br>
- Combinational gate loops formed in netlist
  - Adding small delay will break these loops
  - tool dependent command-line options to generate logs
    * `+vcs+loopreport=100000`
    * `+autofindloop` in questa
- Fixing
  - Different DFF models for clock dividers
  - Delays can be added in gate paths
    - Can be done using SDF annotations
    - `.tfile` will add delays without changing design code
    Note: Using different model will suffice

### Functional bugs with Netlist
- Multi driven signals
  - FV(Formal Verification) will catch it
- Glitches in simulation
  - ![Incorrect latching of enable signal](https://www.edn.com/wp-content/uploads/contenteetimes-images-01mdunn-ic-glitch21.png)
  - ![Converging outputs of flops as clock](https://www.edn.com/wp-content/uploads/contenteetimes-images-01mdunn-ic-glitch22.png)
  - ![Glitch due to reset crossing](https://www.edn.com/wp-content/uploads/contenteetimes-images-01mdunn-ic-glitch23.png)
  - ![Clock signals reconverging on a mux](https://www.edn.com/wp-content/uploads/contenteetimes-images-01mdunn-ic-glitch24.png)
  - ![Using combinational gates for clock gating](https://www.edn.com/wp-content/uploads/contenteetimes-images-01mdunn-ic-glitch25.png)
  Note: [website](https://www.edn.com/design-faults-leading-to-clock-and-data-glitches/)
- Unconnected ports
  - `'hz` *(default value)* causing `x`-propogation
  - Caught in FV
Note: needs research
  - Simulation hang issues
  - Additional functional issues, *found during debug*

### Simulation hang due to TB/model issues
  - Memory data getting corrupted - *initialization*
  - Memory data out signal `X`
  - `x`-propogation from TB
  - Non-resettable flops causing x-propogation

### SDF simulation issues
discrepencies b/w SDF file and design chages should trigger SDF file regeneration netlist and SDF cells and nets names must match

## Websites Used to make
* [Deepchip](http://www.deepchip.com/items/0569-01.html)
* [Gls-Necessary Evil](whatisverification.blogspot.com/2011/06/gate-level-simulations-necessary-evil.html)
* [Gls Environment](https://glsverificationenv.blogspot.com/2012/10/gls-verification-environment-development.html)
* [Clock Glitches](https://www.edn.com/design-faults-leading-to-clock-and-data-glitches/)
- pdfs for futher knowledge
* [Non-Resettable Flops](../Identifying-Non-resettable-flops.pdf)
* [X-propogation](../x-propogation-in-gls.pdf)
